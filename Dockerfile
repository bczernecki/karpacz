#FROM rhub/debian-clang-devel:latest
FROM rocker/r-base:latest
#LABEL karpacz karpacz

ENV PATH=$PATH:/opt/R-devel/bin

RUN ln -sf /opt/R-devel/bin/R* /usr/bin

RUN apt update && apt install -y libxml2 libxml2-dev libssl-dev vim wget gdebi-core libapparmor1 libclang-11-dev \
 libclang-dev libpq5 psmisc sudo git libgit2-dev libssl1.1 gdal-bin libgdal-dev libgdal31 libudunits2-dev \
 lsb-release python3-pip libmagick++-dev pandoc libssh2-1-dev libssl-dev zlib1g-dev libcurl4-openssl-dev make \
 libicu-dev libfontconfig1-dev libharfbuzz-dev libfribidi-dev rsync

# install extra items to work with NetCDF standards:
RUN apt install -y cdo nco libnetcdf19 grads ncview libnetcdf-dev netcdf-bin libpnetcdf-dev

RUN wget https://download2.rstudio.org/server/bionic/amd64/rstudio-server-2022.07.0-548-amd64.deb
RUN dpkg -i rstudio-server-2022.07.0-548-amd64.deb \
 && rm rstudio-server-2022.07.0-548-amd64.deb

USER root

RUN useradd rstudio \
  && echo "rstudio:rstudio" | chpasswd \
	&& mkdir /home/rstudio \
	&& chown rstudio:rstudio /home/rstudio \
	&& addgroup rstudio staff

ENV USER=rstudio

USER root
RUN chown -R rstudio /var/lib/rstudio-server
#chmod -R g=u /var/lib/rstudio-server

# install some extra R packages needed e.g. for building documentation:
RUN R -e 'options(repos = "https://cloud.r-project.org") ; install.packages(c("XML", "httr", "curl", "dplyr", "tidyr", "rmarkdown", "maps",  "climate", "roxygen2", "data.table", "devtools", "thunder", "tmap", "raster", "terra", "ragg", "shiny", "knitr", "pkgdown", "openair", "rnaturalearthdata"), dep = T)'

# install python with metpy and jupyter
RUN pip install metpy jupyterlab jupyter

# setwd
WORKDIR /home/rstudio
EXPOSE 8787 8888 80

# setting multiple CMD or CMD with ENTRYPOINT doesn't work. Creating bash script instead
ADD start.sh /
RUN chmod +x /start.sh
CMD ["/start.sh"]

###################################################
###################################################
# OLDIES/JUNKS:
#CMD ["sh", "-c", "echo \"Rstudio http://localhost:8787\" && /usr/local/bin/jupyter-lab --allow-root --ip 0.0.0.0 --port 8888 --no-browser && /usr/lib/rstudio-server/bin/rserver --server-daemonize 0 --auth-none 1"]
#CMD && jupyter lab --ip=0.0.0.0 --allow root"]
# CMD ["sh", "-c", "echo \"Rstudio http://localhost:8787\" && /usr/lib/rstudio-server/bin/rserver --server-daemonize 0 --auth-none 1"]
# trying ENTRYPOINT and cmd together:
# ENTRYPOINT ["jupyter", "lab","--ip=0.0.0.0","--allow-root"]
###################################################
