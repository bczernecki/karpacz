#!/bin/bash

echo "Rstudio http://localhost:8787"
/usr/lib/rstudio-server/bin/rserver --server-daemonize 0 --auth-none 1 &
sleep 2
/usr/local/bin/jupyter-lab --allow-root --ip 0.0.0.0 --port 8888 --no-browser